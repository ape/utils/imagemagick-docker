# Image Magick with Docker

## Getting Started

### Build

```
# docker build -t imagemagick`
```

### Use

Example, say you want to convert a bunch of pictures from a folder `./images` to another folder `./images/thumbs`, applying various changes:

```
# docker run --rm -t -v $(pwd)/images:/var/imagemagick/images imagemagick \
mogrify -thumbnail '300x300' -gravity center -background '#fcfcfc' -extent '300x300' -path ./thumbs "./*.JPG"
```



